// resolutiontest2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

#include "display_configuration.h"

void set_resolution(int width, int height)
{
	WDDMInterface wddm;

	DISPLAY_DEVICE dd = { 0 };
	dd.cb = sizeof(DISPLAY_DEVICE);
	EnumDisplayDevices(NULL, 0, &dd, 0);

	DEVMODE devmode = { 0 };
	devmode.dmSize = sizeof(DEVMODE);

	EnumDisplaySettings(dd.DeviceName, ENUM_CURRENT_SETTINGS, &devmode);
	devmode.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT;
	devmode.dmPelsWidth = width;
	devmode.dmPelsHeight = height;

	wddm.set_monitors_config(true);
	wddm.custom_display_escape(dd.DeviceName, &devmode);
	wddm.update_display_settings();
}

int main()
{
	set_resolution(824, 624);
	Sleep(1000);
	set_resolution(1234, 1234);
	Sleep(1000);
	set_resolution(1920, 1080);
	Sleep(1000);
}
